import React from 'react';
import ReactDOM from 'react-dom';
import './form.css';

export default  class FlexiForm extends React.Component {
    state ={};
    constructor(props) {
        super(props);
    }

    onSubmit = (e) => {
        e.preventDefault();
        console.log(this.state);
        if (this.props.onSubmit) this.props.onSubmit(this.state);
    }
    handleNameChange = (e) => {
        e.preventDefault();
        this.setState({name: e.target.value});
    }
    handleCityChange = (e) => {
        e.preventDefault();
        this.setState({city: e.target.value});
    }
    renderForm = () => {
        console.log(this.props);
        let model = this.props.config;
        
        let formUI = model.items.map((m) => {
            let label = m.label;
            let type = m.type=="TextField" ? "text": m.type;
            let name= m.name;

            let input =  <input
                    className="form-input"
                    type={type}
                    label={label}
                    value={this.state.name}
                    onChange={this.handleNameChange}
                    required
                    key={label}
                />;

            if (type == "DropDown") {
                input = m.values.map((o) => {
                    console.log("select: ", o);
                     return (
                            <option
                                className="form-input"
                                key={o}
                                value={o}
                            >{o}</option>
                     );
                });

                input = <select required value={this.state.city}
                onChange={this.handleCityChange}>{input}</select>;
             }
            console.log(input);
            return (
                <div className="form-group">
                    <label className="form-label">
                        {m.label}
                    </label>
                    {input}
                </div>
            );
        });
        return formUI;
    }

    render () {
        let title = this.props.title || "React Form";

        return (
            <div className={this.props.className}>
                <h3 className="form-title">{title}</h3>
                <form className="dynamic-form" onSubmit={(e)=>{this.onSubmit(e)}}>
                    {this.renderForm()}
                    <div className="form-actions">
                        <button type="submit">submit</button>
                    </div>
                    
                </form>
                <div>NAME: {this.state.name}</div>
                <div>CITY: {this.state.city}</div>
            </div>
        )
    }
}