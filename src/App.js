import React, { Component } from 'react';
import flexiConfig from './formConfig';
import Flexi from './components/Flexi'

class App extends Component {
  state = {
    
  }

  onSubmit = (model) => {
    console.log(model)
  }

  render() {
    return (
      <div className="App">
        <Flexi onSubmit={this.onSubmit} config={flexiConfig}/>
      </div>
    );
  }
}

export default App;
